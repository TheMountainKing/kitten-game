dojo.declare("classes.KGConfig", null, {
    statics: {
        disableWebWorkers: false,
        locales: ["ru", "ja", "br", "es", "fr", "cz", "zh"],
        isEldermass: false
    }
});
